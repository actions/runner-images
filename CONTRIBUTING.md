# Contribution Guidelines

<details><summary>Table of Contents</summary>

- [Contribution Guidelines](#contribution-guidelines)
  - [Introduction](#introduction)
  - [Developer Certificate of Origin (DCO)](#developer-certificate-of-origin-dco)

</details>

## Introduction

This project is the official Gitea Actions runner images. This document explains how to contribute changes to the project. \
Sensitive security-related issues should be reported to [security@gitea.io](mailto:security@gitea.io).

## Developer Certificate of Origin (DCO)

We consider the act of contributing to the code by submitting a Pull Request as the "Sign off" or agreement to the certifications and terms of the [DCO](DCO) and [MIT license](LICENSE). \
No further action is required. \
You can also decide to sign off your commits by adding the following line at the end of your commit messages:

```
Signed-off-by: Joe Smith <joe.smith@email.com>
```

If you set the `user.name` and `user.email` Git config options, you can add the line to the end of your commits automatically with `git commit -s`.

We assume in good faith that the information you provide is legally binding.
